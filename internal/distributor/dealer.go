package distributor

import (
	"errors"
	"sync"

	"gitlab.com/crylysis/indicators/model"
)

type (
	// Поставщик конечных данных на все конечные узлы клиентов
	// Получает и доставляет данные из конкретного IndicatorProcessor
	Dealer struct {
		source           <-chan *model.IndicatorResult
		clientsConsumers []*ClientConsumer

		mx sync.Mutex
	}

	// Подписка клиента
	ClientConsumer struct {
		ID        string
		consumers []chan<- *model.IndicatorResult

		mx sync.Mutex
	}
)

var (
	ErrConsumerIsNotFinding = errors.New(`consumer is not finding`)
	ErrConsumerIsExist      = errors.New(`consumer is exist`)
)

// Write message for clients
func (cc *ClientConsumer) Write(data *model.IndicatorResult) {
	cc.mx.Lock()
	defer cc.mx.Unlock()

	for _, consumer := range cc.consumers {
		consumer <- data
	}
}

func NewDealer(source chan *model.IndicatorResult) *Dealer {
	return &Dealer{
		source: source,
	}
}

func (d *Dealer) Write(data *model.IndicatorResult) {
	d.write(d.clientsConsumers, data)
}

// Use for recursive write message for all clients
func (d *Dealer) write(consumers []*ClientConsumer, data *model.IndicatorResult) {
	if d == nil {
		return
	}

	d.mx.Lock()
	defer d.mx.Unlock()

	switch len(consumers) {
	case 0:
		return

	case 1:
		go consumers[0].Write(data)
		return

	case 2:
		go consumers[0].Write(data)
		go consumers[1].Write(data)
		return

	default:
		// Recursion write next elements...
		d.write(consumers[2:], data)

		go consumers[0].Write(data)
		go consumers[1].Write(data)
	}
}

// Добавляет клиента в dealer
func (d *Dealer) SetConsumer(consumer *ClientConsumer) error {
	d.mx.Lock()
	defer d.mx.Unlock()

	// Check exist
	for _, clientConsumer := range d.clientsConsumers {
		if clientConsumer.ID == consumer.ID {
			return ErrConsumerIsExist
		}
	}

	d.clientsConsumers = append(d.clientsConsumers, consumer)
	return nil
}

// Удаляет клиента из dealer
func (d *Dealer) DelConsumer(idConsumer string) error {
	d.mx.Lock()
	defer d.mx.Unlock()

	clearConsumersList := make([]*ClientConsumer, 0, len(d.clientsConsumers)-1)

	for _, clientConsumer := range d.clientsConsumers {
		if clientConsumer.ID == idConsumer {
			continue
		}
		clearConsumersList = append(clearConsumersList, clientConsumer)
	}

	if len(clearConsumersList) == len(d.clientsConsumers) {
		return ErrConsumerIsNotFinding
	}

	d.clientsConsumers = clearConsumersList
	return nil
}
