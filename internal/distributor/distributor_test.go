package distributor_test

import (
	"log"
	"testing"

	"github.com/sirupsen/logrus"
	"gitlab.com/crylysis/indicators/internal/distributor"
	"gitlab.com/crylysis/indicators/internal/manager"
	"gitlab.com/crylysis/indicators/model"
)

func TestDistributor(t *testing.T) {
	logger := logrus.New()

	// Инициализация менеджера биржи
	// Иниицализация дистрибьютора
	distr := distributor.NewDistributor(manager.NewMocExchangeManager(), logger)

	t.Run(``, func(t *testing.T) {
		testCreateAndDelete(t, distr)
	})

	t.Run(``, func(t *testing.T) {
		testAppendClientInDistributor(t, distr)
	})
}

var testListIndicators = []distributor.IndicatorDiscriptor{
	{
		InstumentID: &model.FinancialInstrumentID{
			Symbol:   string(manager.SymbolSpotBTCUSDT),
			Interval: model.IntervalKline_MINUTE_1,
		},
		Type: model.IndicatorType_INDICATOR_RSI,
		Configuration: model.RSIConfiguration{
			Size: []int{3, 9},
		},
	},
	{
		InstumentID: &model.FinancialInstrumentID{
			Symbol:   string(manager.SymbolSpotBTCUSDT),
			Interval: model.IntervalKline_MINUTE_1,
		},
		Type: model.IndicatorType_INDICATOR_RSI,
		Configuration: model.RSIConfiguration{
			Size: []int{1, 3},
		},
	},
	{
		InstumentID: &model.FinancialInstrumentID{
			Symbol:   string(manager.SymbolSpotBTCUSDT),
			Interval: model.IntervalKline_MINUTE_1,
		},
		Type: model.IndicatorType_INDICATOR_RSI,
		Configuration: model.RSIConfiguration{
			Size: []int{6, 1},
		},
	},
	{
		InstumentID: &model.FinancialInstrumentID{
			Symbol:   string(manager.SymbolSpotBTCUSDT),
			Interval: model.IntervalKline_MINUTE_1,
		},
		Type: model.IndicatorType_INDICATOR_RSI,
		Configuration: model.RSIConfiguration{
			Size: []int{9, 3},
		},
	},
}

// Действие 1.
// Повторить Шаг.1 и Шаг.2 - 2 раза
// -----------------------------------------
// Шаг.1 Повторить Шаг 1 - 3 раза
// Создание нескольких иммитаций клиентов
// Добавление дискрипторов инидикаторов (2 индикатора)
// Проверка получения данных клиентами
// Шаг.2
// Удаление клиентов
// Удаление индикаторов
func testCreateAndDelete(t *testing.T, dest *distributor.Distibutor) {
	// numClient := 3
	// clients := make([]chan *model.IndicatorResult, numClient)

	ids, err := dest.AddIndicatorProcessors(testListIndicators)
	if err != nil {
		t.Error(err)
		return
	}

	log.Println(ids)
}

// Действие 2.
// -----------------------------------------
// Шаг.1
// Создать нескольких иммитаций клиентов
// Добавление дискрипторов инидикаторов (3 индикатора)
// Добавить один клиент в один индикатор
// Шаг.2
// После получения 5 сообщений добавить клиентов ещё в один индикатор (Повторить пока во всех индикаторах не появятся все клиенты)
func testAppendClientInDistributor(t *testing.T, distr *distributor.Distibutor) {

}
