package distributor

import (
	"errors"
	"fmt"
	"sync"

	"github.com/sirupsen/logrus"
	"gitlab.com/crylysis/indicators/internal/manager"
	"gitlab.com/crylysis/indicators/model"
	indicatorProcessor "gitlab.com/crylysis/indicators/pkg/indicator"
)

type (
	// Distibutor отвечает за:
	// - создание pipeline
	// - добавление новых индикаторов
	// - удаление индикаторов
	// - позволяет получить источник данных от индикаторов (результат их работы )
	Distibutor struct {
		// Клиент для биржи
		exchangeManager manager.ExchangeManager
		// Хранение всех доступных подписчиков на финансовые инструменты
		instrumentsSubscribers map[*model.FinancialInstrumentID]*PipelineManager

		logger *logrus.Logger
		mx     sync.Mutex
	}

	// Группировка подписчика (инструмент) и поставщика (процессор)
	PipelineManager struct {
		pipelineDelivery *manager.DeliveryPipeline
		processors       map[*model.IndicatorID]*IndicatorDistributor
	}

	// TODO: Dealer изменить на интерфейс.
	// 	Dealer Передавать в Processor.
	//	Переименовать Dealer на ClientDealer.

	// Получает данные из индикаторов и отправляет в Диллер (подписанных клиентов).
	IndicatorDistributor struct {
		Processor *manager.ControllerIndicatorProcessor
		Dealer    *Dealer
	}

	IndicatorDiscriptor struct {
		InstumentID   *model.FinancialInstrumentID
		IndicatorID   *model.IndicatorID
		Type          model.IndicatorType `json:"type" yaml:"type"`
		Configuration any                 `json:"configuration" yaml:"configuration"`
	}
)

func (id *IndicatorDiscriptor) GetInstrumentID() *model.FinancialInstrumentID {
	return id.InstumentID
}

func (id *IndicatorDiscriptor) GetIndicatorID() *model.IndicatorID {
	return id.IndicatorID
}

var (
	// TODO: Написать конфигуратор
	IndicatorsDiscriptions = []IndicatorDiscriptor{
		{
			// ID: &model.FinancialInstrumentID{
			// 	Tag: `rsi_5&14`,
			// },
			Type: model.IndicatorType_INDICATOR_RSI,
			Configuration: model.RSIConfiguration{
				Size: []int{5, 14},
			},
		},
	}

	ErrSubscriberIsNotExist = errors.New(`subscriber is not found`)
	ErrSubscriberIsExist    = errors.New(`subscribers is exist`)
)

// Для инициализации менеджера указывается биржа, для которой будут создан менеджер
func NewDistributor(exchangeManager manager.ExchangeManager, logger *logrus.Logger) *Distibutor {
	return &Distibutor{
		exchangeManager:        exchangeManager,
		instrumentsSubscribers: make(map[*model.FinancialInstrumentID]*PipelineManager),

		logger: logger,
		mx:     sync.Mutex{},
	}
}

// Создает новый pipeline для получения данных с биржи
func (pm *Distibutor) CreatePipeline(financialInstrumentID *model.FinancialInstrumentID, pipelineDelivery *manager.DeliveryPipeline) error {
	pm.mx.Lock()
	defer pm.mx.Unlock()

	if _, ok := pm.instrumentsSubscribers[financialInstrumentID]; ok {
		return ErrSubscriberIsExist
	}

	// Формируют список доступных процессоров
	if _, ok := pm.instrumentsSubscribers[financialInstrumentID]; ok {
		return fmt.Errorf(`subscriber exist`)
	}
	pm.instrumentsSubscribers[financialInstrumentID] = &PipelineManager{
		pipelineDelivery: pipelineDelivery,
		processors:       make(map[*model.IndicatorID]*IndicatorDistributor),
	}
	return nil
}

// Добавляет индикатор на существующий subscriber, возвращает идентификатор процессора
func (pm *Distibutor) AddIndicatorProcessors(configs []IndicatorDiscriptor) ([]*model.FinancialInstrumentID, error) {
	filterConfigs := make([]IndicatorDiscriptor, 0)
	processorsIDs := make([]*model.FinancialInstrumentID, 0)

	for _, config := range configs {
		if _, ok := pm.instrumentsSubscribers[config.GetInstrumentID()]; ok {
			continue
		}

		filterConfigs = append(filterConfigs, config)
	}

	processors, err := pm.newProcessors(filterConfigs)
	if err != nil {
		return nil, err
	}

	for idx, processor := range processors {
		if instrumentSubscriber, ok := pm.instrumentsSubscribers[processor.GetInstrumentID()]; ok {
			instrumentSubscriber.pipelineDelivery.AddProcessor(processor)
			instrumentSubscriber.processors[processor.GetIndicatorID()] = &IndicatorDistributor{
				Processor: &processor,
				Dealer:    NewDealer(processor.GetChannelOut()),
			}
		}
		processorsIDs = append(processorsIDs, configs[idx].GetInstrumentID())
	}

	return processorsIDs, nil
}

// Удаляет индикатор по идентификатору
func (pm *Distibutor) DeleteIndicatorProcessorByID() {
}

// Позволяет получить канал для данных индикатора по идентификатору процессора
func (pm *Distibutor) GetDataChannelForIndicatorProcessor(id string) (chan any, error) {
	return nil, nil
}

// Подготавливает необходимый процессор индикатора
func (pm *Distibutor) newProcessors(configs []IndicatorDiscriptor) ([]manager.ControllerIndicatorProcessor, error) {
	indicatorProcessors := make([]manager.ControllerIndicatorProcessor, 0)

	for _, conf := range configs {
		out := make(chan *model.IndicatorResult)
		switch indicator := conf.Configuration.(type) {
		case model.RSIConfiguration:
			indicatorProcessors = append(
				indicatorProcessors,
				indicatorProcessor.NewProcessorRSI(out, indicator.Size...),
			)
		default:
			return nil, fmt.Errorf(`indicator %s don't have configuration %T`, conf.GetIndicatorID().GetTag(), conf.Configuration)
		}

		pm.logger.Debugf(`succeed add indicator processor: %s - %s.%T`, conf.GetIndicatorID().GetTag(), conf.Type.String(), conf.Configuration)
	}

	return indicatorProcessors, nil
}
