package subscribe

import (
	context "context"

	model "gitlab.com/crylysis/indicators/model"
	emptypb "google.golang.org/protobuf/types/known/emptypb"
)

type SubscribeService struct {
	UnimplementedIndicatorsServiceServer
}

func NewSubscribeService() *SubscribeService {
	return &SubscribeService{}
}

// Список доступных индикаторов диржевых сделок
func (ss *SubscribeService) ListExchange(ctx context.Context, _ *emptypb.Empty) (*model.ListExchange, error) {
	listExchange := &model.ListExchange{
		Values: make([]string, 0, len(model.ExchangeType_name)),
	}

	for _, exchange := range model.ExchangeType_name {
		listExchange.Values = append(listExchange.Values, exchange)
	}
	return listExchange, nil
}

// Список доступных инструментов в бирже
func (ss *SubscribeService) ListExchangeFinanceInstruments(ctx context.Context) (*emptypb.Empty, error) {

	return &emptypb.Empty{}, nil
}

// Регистрация подписчика
func (ss *SubscribeService) RegisterSubscriber(ctx context.Context) (*emptypb.Empty, error) {

	return &emptypb.Empty{}, nil
}

// Регистрация клиента для подписчика
func (ss *SubscribeService) AddSubscriberClientForSubscriber(ctx context.Context) (*emptypb.Empty, error) {

	return &emptypb.Empty{}, nil
}

// Получения информации о текущем подписчике, его инструментах и клиентах
func (ss *SubscribeService) AboutSubscriber(ctx context.Context) (*emptypb.Empty, error) {

	return &emptypb.Empty{}, nil
}

func (ss *SubscribeService) Echo(ctx context.Context, msg *model.Echo) (*model.Echo, error) {
	return msg, nil
}
