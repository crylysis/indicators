package service

import (
	"context"
	"errors"
	"fmt"
	"net"
	"net/http"
	"sync"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/sirupsen/logrus"
	"gitlab.com/crylysis/indicators/internal/service/subscribe"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type (
	Server struct {
		serverGRPC *grpc.Server
		serverHTTP *http.Server

		listenForGRPC net.Listener
		listenForHTTP net.Listener

		logger            *logrus.Logger
		cancel            context.CancelFunc
		listenHttpHandler *runtime.ServeMux

		wg *sync.WaitGroup
	}

	HostConfig struct {
		Host string
		Port uint16
	}
)

// for get host:port string
func (h *HostConfig) ToString() string {
	return fmt.Sprintf("%s:%d", h.Host, h.Port)
}

var services = []struct {
	Service        *grpc.ServiceDesc
	Implementation any
}{
	{
		Service:        &subscribe.IndicatorsService_ServiceDesc,
		Implementation: subscribe.NewSubscribeService(),
	},
}

func NewServer(grpcConfig, httpConfig *HostConfig, logger *logrus.Logger) (*Server, error) {
	server := grpc.NewServer()
	mux := runtime.NewServeMux()
	opts := []grpc.DialOption{grpc.WithTransportCredentials(insecure.NewCredentials())}

	for _, service := range services {
		server.RegisterService(service.Service, service.Implementation)
	}

	if err := subscribe.RegisterIndicatorsServiceHandlerFromEndpoint(context.Background(), mux, `localhost:8081`, opts); err != nil {
		return nil, err
	}

	// GRPC configuration
	listenForGRPC, err := net.Listen("tcp", grpcConfig.ToString())
	if err != nil {
		return nil, fmt.Errorf("new listen %s: %w", grpcConfig.ToString(), err)
	}
	logger.Infof("GRPC server host:port %s", grpcConfig.ToString())

	// HTTP gateway configuration
	listenForHTTP, err := net.Listen("tcp", httpConfig.ToString())
	if err != nil {
		return nil, fmt.Errorf("new listen %s %w", httpConfig.ToString(), err)
	}
	logger.Infof("HTTP server host:port %s", httpConfig.ToString())

	return &Server{
		serverGRPC:        server,
		serverHTTP:        &http.Server{Handler: mux},
		listenForGRPC:     listenForGRPC,
		listenForHTTP:     listenForHTTP,
		listenHttpHandler: mux,
		logger:            logger,
		wg:                &sync.WaitGroup{},
	}, nil
}

func (a *Server) Run(ctxApp context.Context) {
	ctx, cancel := context.WithCancel(ctxApp)
	a.cancel = cancel

	a.wg.Add(3)
	go func() {
		defer a.wg.Done()

		// context controll
		<-ctx.Done()
		a.serverGRPC.Stop()
		_ = a.serverHTTP.Shutdown(ctxApp)
	}()

	go func() {
		defer a.wg.Done()

		// listen grpc server
		a.logger.Debug("run GRPC service")
		if err := a.serverGRPC.Serve(a.listenForGRPC); err != nil {
			if errors.Is(err, net.ErrClosed) {
				a.logger.Panicf("stop GRPC server %s", err.Error())
			}
		}
	}()

	go func() {
		defer a.wg.Done()

		// listen http server
		a.logger.Debug("run HTTP service")
		if err := a.serverHTTP.Serve(a.listenForHTTP); err != nil {
			if errors.Is(err, net.ErrClosed) {
				a.logger.Panicf("stop HTTP server %s", err.Error())
			}
		}
	}()
}

// stop service and wait stop all service subprocess
func (a *Server) Stop() {
	a.logger.Debug("stoping service")
	a.cancel()
	a.wg.Wait()
	a.logger.Info("stop service")
}
