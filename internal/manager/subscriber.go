/*
	Система подписок на биржевые данные. Позволяет гибко подвязывать
	требуемые индикаторы на конкретный биржевой инструмент.
*/

package manager

import (
	"errors"

	"gitlab.com/crylysis/indicators/model"
)

type ExchangeMetrics int32

const (
	ExchangeMetricsKLine ExchangeMetrics = iota
	ExchangeMetricsLTKline
)

var (
	ErrUnknownExchangeMetrics = errors.New(`unknown type exchange metrics`)
)

type FinancialInstrumentSubscriber interface {
	Data() *SubscriberDataChan
	SubscribeSymbol(symbol SymbolSpot) error
	RemoveSymbol(symbol SymbolSpot) error
}

type SubscriberDataChan struct {
	Name        string
	DataChannel any
}

type KlineSubscriber struct {
	data            chan *model.KlineUnit
	subscribeTopic  func(topic SymbolSpot, exchangeMetrics ExchangeMetrics, handler func(chan any)) error
	exchangeMetrics ExchangeMetrics
}

func (ks *KlineSubscriber) Data() *SubscriberDataChan {
	return &SubscriberDataChan{
		Name:        `kline`,
		DataChannel: ks.data,
	}
}

// TODO: .
func (ks *KlineSubscriber) RemoveTopic(symbol SymbolSpot) error {
	return nil
}

func (ks *KlineSubscriber) SubscribeSymbol(symbol SymbolSpot) error {
	return ks.subscribeTopic(symbol, ks.exchangeMetrics, func(out chan any) {
		for data := range out {
			if kline, ok := data.(*model.KlineUnit); ok {
				ks.data <- kline
			}
		}
		close(ks.data)
	})
}
