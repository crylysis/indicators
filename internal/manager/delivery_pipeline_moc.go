package manager

import (
	"context"
	"sync"
	"testing"
	"time"

	"gitlab.com/crylysis/indicators/model"
)

const pauseBeforeWriteSimulationData = time.Millisecond * 1

// Moc for Exchange ______________________
type MocExchangeManager struct {
	data chan any
}

func NewMocExchangeManager() *MocExchangeManager {
	return &MocExchangeManager{
		data: make(chan any),
	}
}

func (me *MocExchangeManager) Data() *SubscriberDataChan {
	return &SubscriberDataChan{
		Name:        "test data",
		DataChannel: me.data,
	}
}

func (me *MocExchangeManager) SubscribeSymbol(symbol SymbolSpot) error {
	return nil
}

func (me *MocExchangeManager) RemoveSymbol(symbol SymbolSpot) error {
	return nil
}

func (me *MocExchangeManager) SimulationData(data any) {
	me.data <- data
	time.Sleep(pauseBeforeWriteSimulationData)
	close(me.data)
}

func (me *MocExchangeManager) GetExchangeType() model.ExchangeType {
	return model.ExchangeType_EXCHANGE_UNSPECIFIED
}

func (me *MocExchangeManager) GetKlineSubscribe() *KlineSubscriber {
	data := make(chan *model.KlineUnit)
	return &KlineSubscriber{
		data: data,
		subscribeTopic: func(topic SymbolSpot, exchangeMetrics ExchangeMetrics, handler func(chan any)) error {
			return nil
		},
		exchangeMetrics: ExchangeMetricsKLine,
	}
}

// Moc for processors ____________________
type MocProcessor struct {
	t *testing.T

	isStoped  bool
	cancel    context.CancelFunc
	chanData  func() chan *model.IndicatorResult
	chanClose func()

	mx sync.Mutex
}

func (mp *MocProcessor) Run() {
	if !mp.serviceIsStop() {
		return
	}

	if ok := mp.mx.TryLock(); !ok {
		return
	}
	defer mp.mx.Unlock()

	mp.isStoped = false
}

func (mp *MocProcessor) serviceIsStop() bool {
	mp.mx.Lock()
	defer mp.mx.Unlock()
	return mp.isStoped
}

func (mp *MocProcessor) ChannelIn(data any) {
	if mp.serviceIsStop() {
		return
	}

	mp.chanData() <- &model.IndicatorResult{
		Result: &model.IndicatorResult_Str{
			Str: &model.StringResult{
				Value: data.(string),
			},
		},
	}
}

func (mp *MocProcessor) Stop() error {
	if mp.serviceIsStop() {
		return nil
	}

	if ok := mp.mx.TryLock(); !ok {
		return nil
	}
	defer mp.mx.Unlock()

	mp.isStoped = true
	if mp.cancel != nil {
		mp.cancel()
	}
	if mp.chanClose != nil {
		mp.chanClose()
	}
	return nil
}

func (mp *MocProcessor) GetChannelOut() chan *model.IndicatorResult {
	return mp.chanData()
}

func (mp *MocProcessor) GetInstrumentID() *model.FinancialInstrumentID {
	return &model.FinancialInstrumentID{
		Symbol:   `test_symbol`,
		Interval: model.IntervalKline_MINUTE_1,
	}
}

func (mp *MocProcessor) GetIndicatorID() *model.IndicatorID {
	return &model.IndicatorID{
		Type: model.IndicatorType_INDICATOR_RSI,
		Tag:  ``,
	}
}
