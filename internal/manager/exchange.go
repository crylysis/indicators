/*
	Позволяет получать биржевые котировки и взаимодействовать с адаптерами биржи.
*/

package manager

import (
	"context"
	"sync"

	"gitlab.com/crylysis/indicators/model"
)

// ExchangeClient - base definition of provider actions
type ExchangeClient interface {
	Auth(token string) error

	// kline
	SubscribeKline(context.Context, string) (chan any, error)
	RemoveSubscribeKline(string) error
}

type ExchangeManager interface {
	GetKlineSubscribe() *KlineSubscriber
	GetExchangeType() model.ExchangeType
}

type Exchange struct {
	clientExchange ExchangeClient
	exchangeType   model.ExchangeType

	cancel      context.CancelFunc
	ctxExchange context.Context
	wg          sync.WaitGroup
}

func (ex *Exchange) GetExchangeType() model.ExchangeType {
	return ex.exchangeType
}

// KlineSubscribe have *model.KlineUnit stream data
// GetKlineSubscribe new subscriber for listen kline data from exchange
func (ex *Exchange) GetKlineSubscribe() *KlineSubscriber {
	return &KlineSubscriber{
		data:           make(chan *model.KlineUnit),
		subscribeTopic: ex.subscribeTopic,
	}
}

// KlineSubscribe have *model.LTKlineUnit stream data
// GetLTKlineSubscribe new subscriber for listen ltkline data from exchange
func (ex *Exchange) GetLTKlineSubscribe() *KlineSubscriber {
	return &KlineSubscriber{
		data:           make(chan *model.KlineUnit),
		subscribeTopic: ex.subscribeTopic,
	}
}

// Subscribe to provider
func (ex *Exchange) subscribeTopic(symbol SymbolSpot, exchangeMetrics ExchangeMetrics, handler func(chan interface{})) error {
	var (
		err  error
		data chan interface{}
	)

	switch exchangeMetrics {
	case ExchangeMetricsKLine:
		data, err = ex.clientExchange.SubscribeKline(ex.ctxExchange, string(symbol))
	// case ExchangeMetricsLTKline:
	// 	data, err = ex.clientExchange.SubscribeLTKline(ex.ctxExchange, string(symbol))
	default:
		return ErrUnknownExchangeMetrics
	}

	if err == nil {
		go handler(data)
	}

	return err
}

func (ex *Exchange) Stop() {
	if ex == nil || ex.cancel == nil {
		return
	}

	ex.cancel()
}

func NewExchangeManager(exchangeType model.ExchangeType, clientExchange ExchangeClient) ExchangeManager {
	ctx, cancel := context.WithCancel(context.Background())
	return &Exchange{
		clientExchange: clientExchange,
		cancel:         cancel,
		ctxExchange:    ctx,

		wg: sync.WaitGroup{},
	}
}
