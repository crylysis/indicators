package testdata

import (
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"testing"
	"time"

	"github.com/golang/protobuf/proto"
	"gitlab.com/crylysis/indicators/model"
)

var firstElemts = []float64{54.8, 56.8, 57.85, 59.85, 60.57, 61.1, 62.17, 60.6, 62.35, 62.15, 62.35, 61.45, 62.8, 61.37, 62.5, 62.57, 60.8, 59.37, 60.35, 62.35, 62.17, 62.55, 64.55, 64.37, 65.3, 64.42, 62.9, 61.6, 62.05, 60.05, 59.7, 60.9, 60.25, 58.27, 58.7, 57.72, 58.1, 58.2}

func ConvertTime(t time.Time) uint64 {
	return uint64(t.Unix()) * 1000
}

func TestGen(t *testing.T) {
	resultSlice := &model.ListKlineUnit{
		Items: make([]*model.KlineUnit, 0, 100),
	}

	timeStep := time.Millisecond * 10
	timeNow := time.Now()

	klineUnitsData := make([]*model.KlineUnitData, 0, 10)
	var klineUnit *model.KlineUnit

	countKlineUnit := 0
	for _, price := range firstElemts {
		if countKlineUnit%10 == 0 {
			klineUnit = &model.KlineUnit{}
		}
		countKlineUnit++

		timeStart := timeNow
		timeEnd := timeNow.Add(timeStep)

		klineUnitsData = append(klineUnitsData, &model.KlineUnitData{
			TsStart:     ConvertTime(timeStart),
			TsEnd:       ConvertTime(timeEnd),
			Interval:    `1 min`,
			OpenPrice:   strconv.FormatFloat(price, 'f', 2, 64),
			ClosePrice:  strconv.FormatFloat(price-1, 'f', 2, 64),
			MaxPrice:    strconv.FormatFloat(price+0.5, 'f', 2, 64),
			MinPrice:    strconv.FormatFloat(price-1.5, 'f', 2, 64),
			Volume:      ``,
			TsProcessed: uint64(timeStep.Milliseconds()) * 1000,
		})

		if countKlineUnit%10 == 0 {
			klineUnit.Klines = klineUnitsData
			resultSlice.Items = append(resultSlice.Items, klineUnit)

			klineUnit = &model.KlineUnit{}
			klineUnitsData = make([]*model.KlineUnitData, 0, 10)
		}
	}

	random := rand.NewSource(1)

	if len(resultSlice.Items) < 999 {
		for {
			if len(resultSlice.Items) >= 1000 {
				break
			}

			if countKlineUnit%10 == 0 {
				klineUnit = &model.KlineUnit{}
			}
			countKlineUnit++

			// gen priceFirst
			priceFirst := random.Int63() % 200
			priceSecond := random.Int63() % 100
			price, _ := strconv.ParseFloat(fmt.Sprintf("%d.%d", priceFirst, priceSecond), 64)

			timeStart := timeNow
			timeEnd := timeNow.Add(timeStep)
			klineUnitsData = append(klineUnitsData, &model.KlineUnitData{
				TsStart:     ConvertTime(timeStart),
				TsEnd:       ConvertTime(timeEnd),
				Interval:    `1 min`,
				OpenPrice:   strconv.FormatFloat(price, 'f', 2, 64),
				ClosePrice:  strconv.FormatFloat(price-1, 'f', 2, 64),
				MaxPrice:    strconv.FormatFloat(price+0.5, 'f', 2, 64),
				MinPrice:    strconv.FormatFloat(price-1.5, 'f', 2, 64),
				Volume:      ``,
				TsProcessed: uint64(timeStep.Milliseconds()) * 1000,
			})

			if countKlineUnit%10 == 0 {
				klineUnit.Klines = klineUnitsData
				resultSlice.Items = append(resultSlice.Items, klineUnit)

				klineUnit = &model.KlineUnit{}
				klineUnitsData = make([]*model.KlineUnitData, 0, 10)
			}

		}
	}

	result, err := proto.Marshal(resultSlice)
	if err != nil {
		t.Error(err)
	}

	path := os.Getenv(`IND_PATH`)

	file, err := os.Create(path + `result.pb`)
	if err != nil {
		t.Error(err)
	}
	defer file.Close()

	_, err = file.Write(result)
	if err != nil {
		t.Error(err)
	}
}
