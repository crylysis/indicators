package manager

import (
	"context"
	"sync"

	"gitlab.com/crylysis/indicators/model"

	"github.com/sirupsen/logrus"
)

type (
	DeliveryPipeline struct {
		wg           *sync.WaitGroup
		subscriber   FinancialInstrumentSubscriber
		processors   []ControllerIndicatorProcessor
		cancel       context.CancelFunc
		logger       *logrus.Logger
		priceCounter int

		isRunning bool
		mx        sync.Mutex
	}

	ControllerIndicatorProcessor interface {
		GetInstrumentID() *model.FinancialInstrumentID
		GetIndicatorID() *model.IndicatorID
		Run()
		ChannelIn(data any)
		Stop() error
		GetChannelOut() chan *model.IndicatorResult
	}
)

const (
	TypeChannelDataInterface = iota
	TypeChannelDataKline
)

func NewDeliveryPipeline(subs FinancialInstrumentSubscriber, logger *logrus.Logger) *DeliveryPipeline {
	ctx, cancel := context.WithCancel(context.Background())

	pipeline := &DeliveryPipeline{
		subscriber: subs,
		cancel:     cancel,
		logger:     logger,

		wg: &sync.WaitGroup{},
	}

	go pipeline.run(ctx)
	return pipeline
}

// Call context
func (pip *DeliveryPipeline) Stop() {
	pip.stop()
}

// Use only after cancel context
func (pip *DeliveryPipeline) stop() {
	if pip == nil {
		return
	}
	if !pip.mx.TryLock() {
		return
	}
	defer pip.mx.Unlock()

	if !pip.isRunning {
		return
	}
	pip.isRunning = false

	pip.cancel()
	pip.doProcessCancel(pip.processors)
	pip.wg.Wait()
}

func (pip *DeliveryPipeline) AddProcessor(proc ControllerIndicatorProcessor) {
	if pip == nil {
		return
	}

	pip.mx.Lock()
	defer pip.mx.Unlock()
	// Add and Run
	pip.processors = append(pip.processors, proc)
	proc.Run()
}

func (pip *DeliveryPipeline) setNewPrice(kline *model.KlineUnitData) {
	pip.logger.Debugf("\t%d %d price %s %s", pip.priceCounter, kline.GetTsStart(), kline.GetOpenPrice(), kline.GetClosePrice())
	pip.doProcessWork(pip.processors, kline)
	pip.priceCounter++
}

func (pip *DeliveryPipeline) run(ctx context.Context) {
	if pip == nil {
		return
	}

	if pip.isRunning {
		return
	}
	pip.isRunning = true

	var typeDataFromChannel int

	switch pip.subscriber.Data().DataChannel.(type) {
	case chan *model.KlineUnit:
		typeDataFromChannel = TypeChannelDataKline
	case chan any:
		typeDataFromChannel = TypeChannelDataInterface
	}

	pip.wg.Add(1)
	go func() {
		defer func() {
			pip.wg.Done()
			pip.stop()
		}()

		var oldOpenPrice string

		for {
			select {
			case <-ctx.Done():
				return
			default:
			}

			// Kline data
			if typeDataFromChannel == TypeChannelDataKline {
				select {
				case data, ok := <-pip.subscriber.Data().DataChannel.(chan *model.KlineUnit):
					if !ok || !pip.isRunning {
						return
					}

					// Check next value
					for _, kline := range data.Klines {
						// Continue, if price don't have change
						if oldOpenPrice == kline.OpenPrice {
							continue
						}

						pip.setNewPrice(kline)
						oldOpenPrice = kline.OpenPrice
					}
				default:
				}
			}

			// Any data
			if typeDataFromChannel == TypeChannelDataInterface {
				select {
				case data, ok := <-pip.subscriber.Data().DataChannel.(chan any):
					if !ok || !pip.isRunning {
						return
					}
					pip.doProcessWork(pip.processors, data)
				default:
				}
			}

		}
	}()
}

func (pip *DeliveryPipeline) doProcessWork(processors []ControllerIndicatorProcessor, data interface{}) {
	if pip == nil || !pip.isRunning {
		return
	}

	switch len(processors) {
	case 0:
		return

	case 1:
		processors[0].ChannelIn(data)
		return

	case 2:
		processors[0].ChannelIn(data)
		processors[1].ChannelIn(data)
		return

	default:
		// Recursion write next elements...
		pip.doProcessWork(processors[2:], data)

		processors[0].ChannelIn(data)
		processors[1].ChannelIn(data)
	}
}

func (pip *DeliveryPipeline) doProcessCancel(processors []ControllerIndicatorProcessor) {
	if pip == nil {
		return
	}

	switch len(processors) {
	case 0:
		return

	case 1:
		if err := processors[0].Stop(); err != nil {
			logrus.Error(`stop process: `, err)
		}
		return

	case 2:
		if err := processors[0].Stop(); err != nil {
			logrus.Error(`stop process: `, err)
		}
		if err := processors[1].Stop(); err != nil {
			logrus.Error(`stop process: `, err)
		}
		return

	//
	default:
		// Recursion write next elements...
		pip.doProcessCancel(processors[2:])

		if err := processors[0].Stop(); err != nil {
			logrus.Error(`stop process: `, err)
		}
		if err := processors[1].Stop(); err != nil {
			logrus.Error(`stop process: `, err)
		}
	}
}
