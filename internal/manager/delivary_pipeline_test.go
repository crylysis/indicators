package manager

import (
	"fmt"
	"sync"
	"testing"

	"github.com/sirupsen/logrus"
	"gitlab.com/crylysis/indicators/model"
)

func TestNewPipeline(t *testing.T) {
	// Количество процессоров для pipeline
	roundsTests := []struct {
		DataInChannel string
		NumProccess   int
	}{
		{
			DataInChannel: "round 1",
			NumProccess:   1,
		},
		{
			DataInChannel: "round 2",
			NumProccess:   10,
		},
		{
			DataInChannel: "round 3",
			NumProccess:   50,
		},
		{
			DataInChannel: "round 4",
			NumProccess:   100,
		},
		{
			DataInChannel: "round 5",
			NumProccess:   250,
		},
	}

	for _, round := range roundsTests {
		round := round
		t.Run(fmt.Sprintf("Test pipeline with processes: %d", round.NumProccess), func(t *testing.T) {
			exchange := NewMocExchangeManager()
			pipeline := NewDeliveryPipeline(exchange, logrus.New())

			// Формируют список доступных процессоров
			indicatorsProcessors, chanResult := IndicatorsProcessorsForTest(round.NumProccess, t)
			for _, processor := range indicatorsProcessors {
				pipeline.AddProcessor(processor)
			}

			// Счетчик данных записанных в процессор
			var (
				wg          sync.WaitGroup
				countResult int
			)

			wg.Add(1)
			go func() {
				defer wg.Done()
				exchange.SimulationData(round.DataInChannel)
			}()

			// Check data
			for data := range chanResult {
				if data.GetStr().GetValue() != round.DataInChannel {
					t.Error(`invalid data`)
					continue
				}

				// При записи в один process счётчик countResult увеличивается на единицу
				countResult++
			}

			// Проверка для завершения всех параллельных процессов
			wg.Wait()

			// При правильной отработке pipeline должен передать данные из источника в каждый processor
			if round.NumProccess != countResult {
				t.Error(`invalid counter`)
			}
		})
	}
}

// IndicatorsProcessorsForTest предоставляет тестовый набор процессоров (заглушек)
func IndicatorsProcessorsForTest(num int, t *testing.T) ([]ControllerIndicatorProcessor, chan *model.IndicatorResult) {
	processors := make([]ControllerIndicatorProcessor, num)
	chanProcessors := make(chan *model.IndicatorResult)
	stopChan := make(chan struct{})

	for idx := range processors {
		processors[idx] = &MocProcessor{
			t: t,
			chanData: func() chan *model.IndicatorResult {
				return chanProcessors
			},
			chanClose: func() {
				stopChan <- struct{}{}
			},
		}
	}

	go func() {
		<-stopChan
		close(chanProcessors)
	}()

	return processors, chanProcessors
}
