package manager

import (
	"errors"
	"log"
	"testing"
	"time"

	"gitlab.com/crylysis/indicators/model"
	"gitlab.com/crylysis/indicators/pkg/adapter/bybit"
)

func TestExchange(t *testing.T) {
	var (
		exchangeClient  ExchangeClient
		exchangeManager ExchangeManager
		klineSubscriber *KlineSubscriber
		err             error
		countResponse   int
	)

	t.Run(`Init new exchange`, func(t *testing.T) {
		exchangeClient, err = bybit.NewBybit()
		if err != nil {
			t.Error(err)
		}
	})

	t.Run(`Init new manager`, func(t *testing.T) {
		if err != nil {
			t.Skip()
		}
		exchangeManager = NewExchangeManager(model.ExchangeType_EXHCANGE_BYBIT, exchangeClient)
		if exchangeManager == nil {
			err = errors.New(`invalid manager`)
			t.Error(err)
		}
	})

	t.Run(`Test subscribe`, func(t *testing.T) {
		if err != nil {
			t.Skip()
		}

		klineSubscriber = exchangeManager.GetKlineSubscribe()
		err = klineSubscriber.SubscribeSymbol(SymbolSpotBTCUSDT)
		if err != nil {
			t.Error(err)
		}

		ticker := time.NewTicker(time.Second * 5)

	CLIENT_TEST:
		for {
			select {
			case <-ticker.C:
				break CLIENT_TEST
			case v, ok := <-klineSubscriber.Data().DataChannel.(chan *model.KlineUnit):
				if !ok {
					t.Fatal(`channel from exchange is closed`)
				}

				log.Print(v)

				countResponse++
			}
		}

		if countResponse == 0 {
			t.Error(`counter is null. WSocket not have response`)
			return
		}
	})
}
