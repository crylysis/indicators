# Сервис Indicators

Сервис Indicators - система для подписки на торговые индикаторы.
Сервис Собирает необходи

`Exchange` - bybit or other crypto-exchange.  
`Exchange manager` - universal exchange manager for distributed trade data to `Processors` units  
`Processor(s)` - pipeline of handling indicator algorithms   
`Bus` - historical and real time data of indicators (optional)

![source](/Indicators.webp)


## Подготовка:

Для генерации proto файлов требуется подготовить бинарные файлы в `.generators/bin`:
 - protoc-gen-go
 - protoc-gen-go-grpc
 - protoc-gen-openapiv2
 - protoc-gen-grpc-gateway

Для удобства можно использовать скрипты заготовленные в `.generators`
    
    На текущий момент генерация была проверена только под linux системой.
    Написание скриптов для других систем требуется исключительно для удобства
    разработчиков.


Имея подготовленную систему используйте команду `make proto` для генерации `*.go` файлов и `swagger` документации.
