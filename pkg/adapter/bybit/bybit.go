package bybit

import (
	"context"
	"errors"
	"strings"

	model "gitlab.com/crylysis/indicators/model"

	"github.com/hirokisan/bybit/v2"
	"github.com/sirupsen/logrus"
)

var (
	ErrInvalidToken = errors.New(`invalid token. Token must have key and secret in one string with separator by ":"`)
)

type BybitClient struct {
	bc        *bybit.Client
	klineChan chan *model.KlineUnit
	// ltKlineChan chan *model.KlineUnit

	wsClient         *bybit.WebSocketClient
	wsPubSpotService bybit.V5WebsocketPublicServiceI
	// wsKline  *bybit.V5WebsocketPublicKlineData
	// wsLTKline bybit.V5WebsocketPublicKlineData
}

// Token is `key:secret`
func (bc *BybitClient) Auth(token string) error {
	splitToken := strings.Split(token, ":")
	if len(splitToken) != 0 {
		return ErrInvalidToken
	}

	bc.bc = bc.bc.WithAuth(splitToken[0], splitToken[1])
	return nil
}

func subscribeKlineHandler(klineChan chan any) func(vwpkr bybit.V5WebsocketPublicKlineResponse) error {
	return func(vwpkr bybit.V5WebsocketPublicKlineResponse) error {
		data := make([]*model.KlineUnitData, len(vwpkr.Data))
		for idx, unitData := range vwpkr.Data {
			data[idx] = &model.KlineUnitData{
				TsStart:     uint64(unitData.Start),
				TsEnd:       uint64(unitData.End),
				Interval:    string(unitData.Interval),
				OpenPrice:   unitData.Open,
				ClosePrice:  unitData.Close,
				MaxPrice:    unitData.High,
				MinPrice:    unitData.Low,
				Volume:      unitData.Volume,
				Turnover:    unitData.Turnover,
				Confirm:     unitData.Confirm,
				TsProcessed: uint64(unitData.Timestamp),
			}
		}

		klineChan <- &model.KlineUnit{
			Topic:     vwpkr.Topic,
			Klines:    data,
			TimeStamp: uint64(vwpkr.TimeStamp),
			Type:      vwpkr.Type,
		}
		return nil
	}
}

func (bc *BybitClient) SubscribeKline(ctx context.Context, symbol string) (chan any, error) {
	if bc.wsClient == nil {
		return nil, nil
	}

	// Check symbol for this exchange metrics type
	// TODO: function for check symbol
	var subscribeSymbol bybit.SymbolV5
	if symbol == string(bybit.SymbolV5BTCUSDT) {
		subscribeSymbol = bybit.SymbolV5BTCUSDT
	} else {
		return nil, errors.New(`invalid symbol`)
	}

	// New channel
	lineChan := make(chan any)

	// Set subscribe handler
	_, err := bc.wsPubSpotService.SubscribeKline(bybit.V5WebsocketPublicKlineParamKey{
		Interval: bybit.Interval1,
		Symbol:   subscribeSymbol,
	}, subscribeKlineHandler(lineChan))

	// Start listen exchange
	go bc.wsPubSpotService.Start(ctx, func(isWebsocketClosed bool, err error) {
		// function for cancel
		if isWebsocketClosed {
			logrus.Info(`subscribe ws is closed`)
		}

		if err != nil {
			logrus.Error(err)
		}

	})

	logrus.Debugf(`subscribe to ticker %s`, subscribeSymbol)
	return lineChan, err
}

func (bc *BybitClient) RemoveSubscribeKline(symbol string) error {

	return nil
}

func (bc *BybitClient) SubscribeLTKline(ctx context.Context, symbol string) (chan any, error) {
	return nil, nil
}

func (bc *BybitClient) RemoveSubscribeLTKline(symbol string) error {

	return nil
}
