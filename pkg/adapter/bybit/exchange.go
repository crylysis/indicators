package bybit

import (
	"github.com/hirokisan/bybit/v2"
	model "gitlab.com/crylysis/indicators/model"
)

func NewBybit() (*BybitClient, error) {
	var err error

	bc := &BybitClient{
		bc:       bybit.NewClient(),
		wsClient: bybit.NewWebsocketClient(),

		klineChan: make(chan *model.KlineUnit),
	}

	bc.wsPubSpotService, err = bc.wsClient.V5().Public(bybit.CategoryV5Spot)
	if err != nil {
		return nil, err
	}

	return bc, nil
}
