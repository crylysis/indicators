// go:build integration
package bybit

import (
	"context"
	"log"
	"testing"
	"time"

	"github.com/hirokisan/bybit/v2"
)

func TestBybitClient(t *testing.T) {
	var countResponse int
	client, _ := NewBybit()
	ctx, cancel := context.WithCancel(context.Background())

	// Subscribe
	data, err := client.SubscribeKline(ctx, string(bybit.SymbolV5BTCUSDT))
	if err != nil {
		t.Error(err)
		cancel()
		return
	}

	ticker := time.NewTicker(time.Second * 2)

CLIENT_TEST:
	for {
		select {
		case <-ticker.C:
			cancel()
			break CLIENT_TEST
		case v, ok := <-data:
			if !ok {
				t.Fatal(`channel from exchange is closed`)
			}

			log.Print(v)

			countResponse++
		}
	}

	if countResponse == 0 {
		t.Error(`counter is null. WSocket not have response`)
		return
	}
	time.Sleep(time.Second * 3)
}
