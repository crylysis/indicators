package indicator

import (
	"context"
	"fmt"
	"math"
	"sync"

	model "gitlab.com/crylysis/indicators/model"

	"github.com/sirupsen/logrus"
)

type ConstrollerProcessorRSI struct {
	InstrumentID *model.FinancialInstrumentID
	IndicatorID  *model.IndicatorID
	rsiQueue     []*RSIPriceQueue

	In  chan *model.RSIPriceUnit
	Out chan *model.IndicatorResult

	isRunning bool
	cancel    context.CancelFunc
	mx        sync.Mutex
}

// TODO: out chan for processorModel
func NewProcessorRSI(out chan *model.IndicatorResult, rsiSizes ...int) *ConstrollerProcessorRSI {
	if len(rsiSizes) < 1 || len(rsiSizes) > 100 {
		return nil
	}

	rsiQueues := make([]*RSIPriceQueue, 0, len(rsiSizes))

	for _, size := range rsiSizes {
		queueProcess := NewRSIPriceQueue(size)
		rsiQueues = append(rsiQueues, queueProcess)
	}

	rsiProcessor := &ConstrollerProcessorRSI{
		rsiQueue: rsiQueues,
		In:       make(chan *model.RSIPriceUnit),
		Out:      out,
	}

	rsiProcessor.Run()
	return rsiProcessor
}

func (ip *ConstrollerProcessorRSI) GetInstrumentID() *model.FinancialInstrumentID {
	return ip.InstrumentID
}

func (ip *ConstrollerProcessorRSI) GetIndicatorID() *model.IndicatorID {
	return ip.IndicatorID
}

func (ip *ConstrollerProcessorRSI) GetChannelOut() chan *model.IndicatorResult {
	return ip.Out
}

func (ip *ConstrollerProcessorRSI) ChannelIn(data any) {
	if !ip.isRunning {
		return
	}
	if klineUnitPrice, ok := data.(*model.KlineUnitData); ok {
		ip.In <- model.TransformKlineToRSI(klineUnitPrice)
	}
}

func (ip *ConstrollerProcessorRSI) Stop() error {
	if ip == nil {
		return nil
	}

	ip.isRunning = false
	ip.cancel()
	return nil
}

func (ip *ConstrollerProcessorRSI) Run() {
	if ip == nil {
		return
	}

	if !ip.mx.TryLock() {
		return
	}
	defer ip.mx.Unlock()

	if ip.isRunning {
		return
	}
	ip.isRunning = true

	processorChans := make([]chan *model.RSIPriceUnit, len(ip.rsiQueue))

	ctx, cancel := context.WithCancel(context.Background())
	for idx, queueProcessor := range ip.rsiQueue {
		procChan := make(chan *model.RSIPriceUnit, 1)
		queueProcessor.Run(ctx, procChan, ip.Out)
		processorChans[idx] = procChan
	}

	go func() {
		// processorChans := processorChans
		for v := range ip.In {
			ip.writePrice(processorChans, v)
		}
	}()

	ip.cancel = cancel
}

func (ip *ConstrollerProcessorRSI) writePrice(procChans []chan *model.RSIPriceUnit, price *model.RSIPriceUnit) {
	if ip == nil || !ip.isRunning {
		return
	}

	switch len(procChans) {
	case 0:
		return

	case 1:
		procChans[0] <- price
		return

	case 2:
		procChans[0] <- price
		procChans[1] <- price
		return

	default:
		// Recursion write next elements...
		ip.writePrice(procChans[2:], price)

		procChans[0] <- price
		procChans[1] <- price
	}
}

type RSIPriceList struct {
	List     []*model.RSIPriceUnit
	MaxPrice float64
	MinPrice float64
}

type RSIPriceQueueUnit struct {
	Current  *model.RSIPriceUnit
	Next     *RSIPriceQueueUnit
	Previous *RSIPriceQueueUnit
}

type RSIPriceQueue struct {
	length    int
	current   int
	rsiSize   int
	price     *RSIPriceQueueUnit
	lastPrice *RSIPriceQueueUnit
	lastUc    float64
	lastDc    float64
	firstWSM  bool
}

func NewRSIPriceQueue(rsiSizes int) *RSIPriceQueue {
	return &RSIPriceQueue{
		rsiSize: rsiSizes,
		length:  rsiSizes * 3,
		price:   nil,
		current: 0,
	}
}

func (pq *RSIPriceQueue) addPrice(price *model.RSIPriceUnit) {
	// Create first price in queue
	if pq.price == nil {
		pq.price = &RSIPriceQueueUnit{
			Current:  price,
			Next:     nil,
			Previous: nil,
		}
		return
	}

	// Add new element
	bufPrice := *pq.price
	pq.price = &RSIPriceQueueUnit{
		Current: price,
		Next:    &bufPrice,
	}
	if bufPrice.Current.OpenPrice == pq.price.Current.OpenPrice &&
		bufPrice.Current.ClosePrice == pq.price.Current.ClosePrice {
		return
	}
	bufPrice.Previous = pq.price

	// Check limit reached
	if pq.current == pq.length {
		pq.delLast()
	} else {
		pq.current++
	}
}

func (pq *RSIPriceQueue) delLast() {
	if pq == nil || pq.lastPrice == nil {
		return
	}

	// Delete last element and shift point "lastPrice" on previous element
	bufPrice := *pq.lastPrice.Previous
	bufPrice.Next = nil
	pq.lastPrice = &bufPrice
}

func (pq *RSIPriceQueue) rsiSummator() (rsi float64) {
	if pq == nil {
		return 0.0
	}

	var rs, uc, dc float64

	// Uc - up difference counter
	// Dc - down difference counter

	if !pq.firstWSM {
		pq.firstWSM = true
		uc, dc, _ = recursiveCalculatePriceDifference(pq.price, pq.rsiSize-1)
		if dc == 0 {
			dc = 100.0
		}

		uc = uc / float64(pq.rsiSize)
		dc = dc / float64(pq.rsiSize)

	} else {
		uc, dc, _ = recursiveCalculatePriceDifference(pq.price, 0)

		uc = (pq.lastUc*float64(pq.rsiSize-1) + uc) / float64(pq.rsiSize)
		dc = (pq.lastDc*float64(pq.rsiSize-1) + dc) / float64(pq.rsiSize)
		uc = roundTo(uc, 2)
		dc = roundTo(dc, 2)
	}

	rs = roundTo(uc/dc, 2)

	pq.lastUc = uc
	pq.lastDc = dc

	rsi = 100 - (100 / (1 + rs))
	rsi = math.Round(rsi*100) / 100

	return rsi
}

func recursiveCalculatePriceDifference(next *RSIPriceQueueUnit, count int) (uc float64, dc float64, defference float64) {
	if count > 0 && next != nil {
		uc, dc, _ = recursiveCalculatePriceDifference(next.Next, count-1)
	}

	currentPrice := next.Current
	nextPrice := next.Next.Current

	defference = (currentPrice.OpenPrice - nextPrice.OpenPrice)
	defference = math.Round(defference*100) / 100

	if defference > 0 {
		uc += defference
		uc = math.Round(uc*100) / 100
	}
	if defference < 0 {
		dc += math.Abs(defference)
		dc = math.Round(dc*100) / 100
	}

	return
}

func (pq *RSIPriceQueue) Run(ctx context.Context, priceChan <-chan *model.RSIPriceUnit, rsiChan chan<- *model.IndicatorResult) {
	if pq == nil {
		return
	}

	go func() {
		for {
			select {
			// Interrupt worker
			case <-ctx.Done():
				logrus.Infof("RSI worker %d close", pq.rsiSize)
				return
			default:
			}

			select {
			// Price constructor
			case price, ok := <-priceChan:
				if !ok {
					logrus.Infof("RSI worker %d close", pq.rsiSize)
					return
				}

				// Add new price
				pq.addPrice(price)

				// Sum RSI on now moment
				if pq.current >= pq.rsiSize {
					rsi := pq.rsiSummator()
					rsiChan <- &model.IndicatorResult{
						// TODO: name formatt standart
						Tag: fmt.Sprintf(`rsi - %d`, pq.rsiSize),
						Result: &model.IndicatorResult_Rsi{
							Rsi: &model.RSIUnit{
								CandleId: price.ID,
								Time:     uint64(price.TSClose.Unix()),
								Rsi:      rsi,
							},
						},
					}
				}

			// Interrupt worker
			case <-ctx.Done():
				logrus.Infof("RSI worker %d close", pq.rsiSize)
				return
			default:
			}
		}

	}()
}
