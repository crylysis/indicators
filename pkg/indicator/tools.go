package indicator

import "math"

// Позволяет произвести прваильное округление для индикаторов: RSI, Средняя.
func roundTo(n float64, decimals uint32) float64 {
	return math.Round(n*math.Pow(10, float64(decimals))) / math.Pow(10, float64(decimals))
}
