PROJECT_DIR = .

proto: clean
	buf generate --exclude-path=third_party/google
	cp ./service/subscribe/* ./internal/service/subscribe
	find ./model -type f '(' -name '*.swagger.json' ')' -exec mv -t ./internal/service/swagger/models/ {} +
	rm -rf ./service

clean: 
	find . -type f '(' -name '*.pb.go' -o -name '*.pb.*.go' -o -name '*.swagger.json' ')' -not -path './vendor/*' -delete