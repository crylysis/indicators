package app

import (
	"context"
	"os"
	"os/signal"
	"syscall"

	"github.com/sirupsen/logrus"
	"gitlab.com/crylysis/indicators/internal/service"
	"gitlab.com/crylysis/indicators/tools/loggo"
)

const (
	defaultGRPCHost string = `0.0.0.0`
	defaultGRPCPort uint16 = 8081
	defaultHTTPHost string = `0.0.0.0`
	defaultHTTPPort uint16 = 8080
)

type IndicatorsApp struct {
	Server *service.Server
}

func App() {
	quit := make(chan os.Signal, 1)
	osKillSignals := []os.Signal{
		syscall.SIGKILL,
		syscall.SIGTERM,
		syscall.SIGHUP,
		syscall.SIGINT,
	}
	signal.Notify(quit, osKillSignals...)

	levelLog := logrus.TraceLevel
	logger := loggo.InitCustomLogger(&logrus.JSONFormatter{TimestampFormat: "15:04:05 02/01/2006"}, levelLog, false, true)

	// init grpc and http gateway
	appServer, err := service.NewServer(
		&service.HostConfig{Host: defaultGRPCHost, Port: defaultGRPCPort},
		&service.HostConfig{Host: defaultHTTPHost, Port: defaultHTTPPort},
		logger,
	)
	if err != nil {
		logger.Fatal(err)
	}

	// init pipeline
	// appPipeline := NewAppPipeline(logger)
	// if err := appPipeline.PipelineConfiguration(IndicatorsConfigurations); err != nil {
	// 	logger.Fatal(`pipeline configuration`, err)
	// }

	ctx := context.Background()

	// run service
	appServer.Run(ctx)

	<-quit
	// Add App run and stop init function
	appServer.Stop()
}
