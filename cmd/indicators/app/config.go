package app

type (
	AppConfig struct {
		HTTP ServerConfig `yaml:"http_server" env_prefix:"HTTP_"`
		GRPC ServerConfig `yaml:"grpc_server" env_prefix:"GRPC_"`
	}

	ServerConfig struct {
		Hostname string `yaml:"host" env:"HOST"`
		Port     uint16 `yaml:"port" env:"PORT"`
	}
)
