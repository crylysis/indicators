package app

// type AppPipeline struct {
// 	logger   *logrus.Logger
// 	pipeline *manager.DeliveryPipeline
// }

// func NewAppPipeline(logger *logrus.Logger) *AppPipeline {
// 	return &AppPipeline{logger: logger}
// }

// func (a *AppPipeline) StopPipepine() {
// 	a.pipeline.Stop()
// }

// func (a *AppPipeline) prepareProcessors(out chan any, configs []IndicatorConfig) ([]model.IndicatorProcessor, error) {
// 	indicatorProcessors := make([]model.IndicatorProcessor, 0)

// 	for _, conf := range configs {
// 		switch indicator := conf.Configuration.(type) {
// 		case model.RSIConfiguration:
// 			indicatorProcessors = append(
// 				indicatorProcessors,
// 				processor.NewProcessorRSI(out, indicator.Size...),
// 			)
// 		default:
// 			return nil, fmt.Errorf(`indicator %s don't have configuration %T`, conf.Tag, conf.Configuration)
// 		}

// 		a.logger.Infof(`configuration indicator: %s - %s ready`, conf.Tag, IndicatorTypeValue[conf.Type])
// 	}

// 	return indicatorProcessors, nil
// }
