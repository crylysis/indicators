package model

import (
	"strconv"
	"time"
)

func ExchangePriceToRSIPriceUnit(data any) []*RSIPriceUnit {
	switch v := data.(type) {
	case *KlineUnit:
		klineDatas := make([]*RSIPriceUnit, len(v.Klines))
		for idx, klineData := range v.Klines {
			klineDatas[idx] = TransformKlineToRSI(klineData)
		}
		return klineDatas
	}

	return nil
}

func TransformKlineToRSI(kline *KlineUnitData) *RSIPriceUnit {
	tsStart := time.Unix(int64(kline.GetTsStart()/1000), int64(kline.GetTsStart()%1000))
	tsEnd := time.Unix(int64(kline.GetTsEnd()/1000), int64(kline.GetTsEnd()%1000))
	priceOpen, _ := strconv.ParseFloat(kline.GetOpenPrice(), 64)
	priceClose, _ := strconv.ParseFloat(kline.GetClosePrice(), 64)
	priceMin, _ := strconv.ParseFloat(kline.GetMinPrice(), 64)
	priceMax, _ := strconv.ParseFloat(kline.GetMaxPrice(), 64)

	return &RSIPriceUnit{
		ID:             strconv.FormatUint(kline.GetTsProcessed(), 10),
		TSOpen:         tsStart,
		TSClose:        tsEnd,
		OpenPrice:      priceOpen,
		ClosePrice:     priceClose,
		MinPrice:       priceMin,
		MaxPrice:       priceMax,
		Volume:         kline.Volume,
		Quote:          kline.Turnover,
		NumberOfTrades: int64(kline.GetTsProcessed()),
		Confirm:        kline.Confirm,
	}
}
