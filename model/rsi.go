package model

import (
	"time"
)

type RSIPriceUnit struct {
	ID             string
	PairID         string
	TSOpen         time.Time
	TSClose        time.Time
	OpenPrice      float64
	ClosePrice     float64
	MinPrice       float64
	MaxPrice       float64
	Volume         string
	Quote          string
	NumberOfTrades int64
	TakerBuyBase   string
	TakerBuyQuote  string
	Confirm        bool
}
